﻿using Moq;
using System.Net.Http;
using System.Security.Principal;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Results;
using WarsawCityGamesServer.DataAccess.DataAccessServices.Instances;
using WarsawCityGamesServer.DataAccess.DataAccessServices.Interfaces;
using WarsawCityGamesServer.Entities.Context;
using WarsawCityGamesServer.Entities.Entities;
using WarsawCityGamesServer.Models.Missions;
using WarsawCityGamesServer.Services.Controllers;
using Xunit;

namespace WarsawCityGamesServer.Tests
{
    public class MissionsTests
    {
        private readonly MissionsController controller;
        private readonly Player player;

        public MissionsTests()
        {
            var levels = new[]
            {
                new Level() {Id = 1, ExpRequired = 0, Name = "Default"}
            };
            var users = new[]
            {
                new User()
                {
                    Email = "jablonskim@gmail.com",
                    UserName = "jablonskim"
                }
            };
            var missions = new[]
            {
                new Mission()
                {
                     Name = "NAME",
                     Description = "DESC",
                     ExpReward = 200,
                     MinimumLevel = levels[0]
                }
            };
            var players = new[]
            {
                new Player()
                {
                    CurrentMission = missions[0], Description = "desc", Exp = 150, Name = "Mateusz Jabłoński", Id = 1,
                    UserImage = new byte[] {3}, Level = levels[0], User = users[0]
                }
            };

            player = players[0];
            var mockContext = new Mock<CityGamesContext>() { CallBase = true };
            mockContext.Setup(c => c.Players).ReturnsDbSet(players);
            mockContext.Setup(c => c.Levels).ReturnsDbSet(levels);
            mockContext.Setup(c => c.Missions).ReturnsDbSet(missions);
            var mockUnitOfWork = MockHelper.MockUnitOfWork(mockContext.Object);
            IMissionService service = new MissionService(mockUnitOfWork.Object);
            IMissionHistoryService hservice = new MissionHistoryService(mockUnitOfWork.Object);
            controller = new MissionsController(service, hservice);

            var mockC = new Mock<HttpControllerContext>();
            controller.ControllerContext = mockC.Object;
            controller.ControllerContext.RequestContext.Principal = new GenericPrincipal(new GenericIdentity("jablonskim", "Type"), new string[] { });
            controller.Configuration = new HttpConfiguration();
            controller.Request = new HttpRequestMessage();
        }

        [Fact]
        public async void AbortCurrentMission_ShouldAbortPlayerCurrentMission()
        {
            var res = await controller.AbortCurrentMission();
            Assert.NotNull(res);
            Assert.Null(player.CurrentMission);
        }


        [Fact]
        public async void SetCurrentMission_ShouldSetPlayerCurrentMission()
        {
            PlayerMissionDto dto = new PlayerMissionDto
            {
                MissionName = "NAME",
                UserName = "jablonskim"
            };
            var res = await controller.SetCurrentMission(dto);
            Assert.NotNull(player.CurrentMission);
        }
    }
}


